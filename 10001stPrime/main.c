#include <stdio.h>

long is_prime(long n)
{
    if(n < 2) return 0;
    long factor = 2;

    while(factor * factor <= n)
    {
        if(n % factor == 0) return 0;
        factor++;
    }
    return 1;
}

long next_prime(long n)
{
    if(!(n & 1)) n--;
    for(;;)
    {
        n += 2;
        if(is_prime(n)) return n;
    }
}

int main()
{
    long p = 2;
    for(int i = 0; i < 10000; i++)
    {
        p = next_prime(p);
    }

    printf("%ld\n", p);

    return 0;
}
