#include <stdio.h>

long is_prime(long n)
{
    if(n < 2) return 0;
    long factor = 2;

    while(factor * factor <= n)
    {
        if(n % factor == 0) return 0;
        factor++;
    }
    return 1;
}

long next_prime(long n)
{
    if(!(n & 1)) n--;
    for(;;)
    {
        n += 2;
        if(is_prime(n)) return n;
    }
}

int main()
{
    long n = 600851475143;
    long factor = 2;
    long largest = 2;
    while(factor * factor <= n)
    {
        if(n % factor == 0)
        {
            if(factor > largest) largest = factor;
            long other_factor = n / factor;
            if(is_prime(other_factor) && other_factor > largest) largest = other_factor;
        }
        factor = next_prime(factor);
    }

    printf("%ld\n", largest);

    return 0;
}
