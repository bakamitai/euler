#include <stdio.h>

char pyramid[] = {
    75, 95, 64, 17, 47, 82, 18, 35, 87, 10, 20, 4, 82, 47, 65, 19, 1, 23, 75, 3,
    34, 88, 2, 77, 73, 7, 63, 67, 99, 65, 4, 28, 6, 16, 70, 92, 41, 41, 26, 56,
    83, 40, 80, 70, 33, 41, 48, 72, 33, 47, 32, 37, 16, 94, 29, 53, 71, 44, 65, 25,
    43, 91, 52, 97, 51, 14, 70, 11, 33, 28, 77, 73, 17, 78, 39, 68, 17, 57, 91, 71,
    52, 38, 17, 14, 91, 43, 58, 50, 27, 29, 48, 63, 66, 4, 68, 89, 53, 67, 30, 73,
    16, 69, 87, 40, 31, 4, 62, 98, 27, 23, 9, 70, 98, 73, 93, 38, 53, 60, 4, 23 
};

typedef struct
{
    char *start;
    size_t length;
} Slice;

typedef struct
{
    Slice row;
    size_t index;
    unsigned value;
} Path;

typedef struct
{
    Path *data;
    size_t length;
} Stack;

Path pop(Stack *s)
{
    return s->data[--s->length];
}

void push(Stack *s, Path p)
{
    s->data[s->length++] = p;
}

int main()
{
    Path paths[16384];
    Stack s;
    s.length = 0;
    s.data = paths;

    Slice root;
    root.start = pyramid;
    root.length = 1;

    Path p;
    p.row = root;
    p.index = 0;
    p.value = p.row.start[p.index];

    push(&s, p);

    unsigned max = 0;
    while(s.length)
    {
        p = pop(&s);

        if(p.row.length == 15)
        {
            if(p.value > max) max = p.value;
        }
        else
        {
            Slice next_row;
            next_row.start = p.row.start + p.row.length;
            next_row.length = p.row.length + 1;

            Path next;
            next.row = next_row;
            next.index = p.index;
            next.value = p.value + next_row.start[p.index];

            push(&s, next);

            next.index = p.index + 1;
            next.value = p.value + next_row.start[p.index + 1];

            push(&s, next);
        }
    }

    printf("%u\n", max);
    return 0;
}
