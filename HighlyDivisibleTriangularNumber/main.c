#include <stdio.h>

unsigned number_of_divisors(unsigned n)
{
    unsigned div = 2;
    unsigned num = 2; // All numbers greater than one have at least 2 divisors
    while(div * div < n)
    {
        num += 2 * ((n % div++) == 0);
    }

    return num + (div * div == n); // Make sure not to count the sqrt twice
}

int main()
{
    unsigned n = 1;
    unsigned tri_num = 0;

    do
    {
        tri_num = (n * (n + 1)) / 2;
        n++;
    } while(number_of_divisors(tri_num) <= 500);

    printf("%u\n", tri_num);
    return 0;
}
