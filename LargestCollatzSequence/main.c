#include <stdio.h>

int main()
{
    unsigned largest_sequence = 0, sequence = 0, largest_number = 0, number = 0;
    
    for(unsigned i = 14; i < 1000000; i++)
    {
        number = i;
        sequence = 1;
        while(number > 1)
        {
            if(number & 1)
            {
                number = (3 * number + 1) / 2;
                sequence += 2;
            }
            else
            {
                number /= 2;
                sequence++;
            }
        }
        if(sequence > largest_sequence)
        {
            largest_sequence = sequence;
            largest_number = i;
        }
    }

    printf("%u\n", largest_number);
    return 0;
}
