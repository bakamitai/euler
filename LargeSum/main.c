#include <stdio.h>
#include <string.h>

void decimal_string_add(char *a, char *b)
{
    for(int i = 0; i < 50; i++)
    {
        int a_val = a[i] - '0', b_val = b[i] - '0', carry, offset = 1;
        a[i] = ((a_val + b_val) % 10) + '0';
        carry = (a_val + b_val) / 10;

        while(carry)
        {
            if(a[i + offset] == '\0') a[i + offset] = '0';

            a_val = a[i + offset] - '0';
            a[i + offset] = (a_val + carry) % 10 + '0';
            carry = (a_val + carry) / 10;
            offset++;
        }
    }
}

void reverse(char *bug, int length)
{
    for(int i = 0; i < length / 2; i++)
    {
        char temp = bug[i];
        bug[i] = bug[length - 1 - i];
        bug[length - 1 - i] = temp;
    }
}

int main(int argc, char **argv)
{
    if(argc == 2)
    {
        FILE *f = fopen(argv[1], "r");
        if(f)
        {
            char temp[50] = {0};
            char acc[52] = {0}; // The largest possible sum of 100 50 digit numbers is 52 digits long

            fread(acc, 1, 50, f);
            reverse(acc, 50);

            while(fread(temp, 1, 50, f) == 50)
            {
                reverse(temp, 50);
                decimal_string_add(acc, temp);
            }
            fclose(f);

            int len = strlen(acc);
            reverse(acc, len);

            // Using puts or printf causes some weird bytes to be printed that are not
            // printed when I do this. I have no idea why.
            for(int i = 0; i < len; putchar(acc[i++]));
            putchar('\n');
        }
    }
    else
    {
        printf("Where's the args babe?\n");
    }

    return 0;
}
