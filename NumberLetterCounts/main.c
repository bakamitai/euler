#include <stdio.h>

// |-----------+-------------------|
// | Word      | Number of letters |
// |-----------+-------------------|
// | one       |                 3 |
// | two       |                 3 |
// | three     |                 5 |
// | four      |                 4 |
// | five      |                 4 |
// | six       |                 3 |
// | seven     |                 5 |
// | eight     |                 5 |
// | nine      |                 4 |
// | ten       |                 3 |
// | eleven    |                 6 |
// | twelve    |                 6 |
// |-----------+-------------------|
// | thirteen  |                 8 |
// | fourteen  |                 8 |
// | fifteen   |                 7 |
// | sixteen   |                 7 |
// | seventeen |                 9 |
// | eighteen  |                 8 |
// | nineteen  |                 8 |
// |-----------+-------------------|
// | twenty    |                 6 |
// | thirty    |                 6 |
// | forty     |                 5 |
// | fifty     |                 5 |
// | sixty     |                 5 |
// | seventy   |                 7 |
// | eighty    |                 6 |
// | ninety    |                 6 |
// |-----------+-------------------|
// | hundred   |                 7 |
// | and       |                 3 |
// | thousand  |                 8 |
// |-----------+-------------------|

int main()
{
    int less_than_teen[12] = {3, 3, 5, 4, 4, 3, 5, 5, 4, 3, 6, 6};
    int teen[7] = {8, 8, 7, 7, 9, 8, 8};
    int tens[8] = {6, 6, 5, 5, 5, 7, 6, 6};
    int hundred = 7;
    int and = 3;
    int thousand = 8;

    unsigned characters = 0;
    for(int i = 1; i < 1000; i++)
    {
        if(i < 13) characters += less_than_teen[i - 1];
        else if(i < 20) characters += teen[i - 13];
        else if(i < 100)
        {
            int t = i / 10;
            characters += tens[t - 2];

            int o = i % 10;
            if(o) characters += less_than_teen[o - 1];
        }
        else
        {
            int h = i / 100;
            characters += less_than_teen[h - 1];
            characters += hundred;

            if(i % 100)
            {
                characters += and;
                int remainder = i % 100;
                
                if(remainder < 13) characters += less_than_teen[remainder - 1];
                else if(remainder < 20) characters += teen[remainder - 13];
                else
                {
                    int t = remainder / 10;
                    characters += tens[t - 2];

                    int o = remainder % 10;
                    if(o) characters += less_than_teen[o - 1];
                }
            }
        }
    }

    characters += less_than_teen[0];
    characters += thousand;

    printf("%u\n", characters);
    return 0;
}
