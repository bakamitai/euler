// This one felt a little too easy. I think there's probably a more optimal way to calculate this.
#include <stdio.h>

int main()
{
    int sum1 = 0, sum2 = 0;
    
    for(int i = 1; i < 101; i++)
    {
        sum1 += i * i;
        sum2 += i;
    }
    sum2 *= sum2;
    printf("%d\n", sum2 - sum1);
    return 0;
}
