#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>

typedef struct
{
    char *data;
    size_t length;
} String;

String global_s;
static uint64_t integer_permutations[3628800] = {0};
static int index = 0;

void print_string(String s)
{
    for(int i = 0; i < s.length; i++)
    {
        putchar(s.data[i]);
    }
    putchar('\n');
}

int global_permutations = 1;

void quicksort(int start, int end)
{
    if(end > start)
    {
        int pivot = (start + end) / 2;
        uint64_t pivot_value = integer_permutations[pivot];
        integer_permutations[pivot] = integer_permutations[end];
        integer_permutations[end] = pivot_value;
        int part_end = start;

        for(int i = start; i < end; i++)
        {
            if(integer_permutations[i] < pivot_value)
            {
                uint64_t temp = integer_permutations[i];
                integer_permutations[i] = integer_permutations[part_end];
                integer_permutations[part_end] = temp;
                part_end++;
            }
        }
        integer_permutations[end] = integer_permutations[part_end];
        integer_permutations[part_end] = pivot_value;
        quicksort(start, part_end - 1);
        quicksort(part_end + 1, end);
    }
}

void permutations(String s)
{
    if(s.length > 1)
    {
        String next;
        next.data = s.data + 1;
        next.length = s.length - 1;

        permutations(next);
        for(int i = 1; i < s.length; i++)
        {
            char temp = s.data[0];
            s.data[0] = s.data[i];
            s.data[i] = temp;

            permutations(next);
            temp = s.data[0];
            s.data[0] = s.data[i];
            s.data[i] = temp;
        }
    }
    else
    {
        integer_permutations[index++] = atol(global_s.data);
    }
}

int main()
{
    char n[] = "0123456789";
    global_s.data = n;
    global_s.length = sizeof(n) - 1;
    String s = global_s;
    permutations(s);
    quicksort(0, 3628800 - 1);
    printf("%lu\n", integer_permutations[999999]);
    
    return 0;
}
