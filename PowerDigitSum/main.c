#include <stdio.h>
#include <stdlib.h>

typedef struct
{
    char *data;
    size_t length;
    size_t capacity;
} Number;

void number_realloc(Number *n)
{
    n->capacity *= 2;
    n->data = realloc(n->data, n->capacity);
    for(int i = n->length; i < n->capacity; i++) n->data[i] = 0;
}

void decimal_number_add(Number *a, Number *b)
{
    for(int i = 0; i < b->length; i++)
    {
        int a_val = a->data[i], carry = 0, b_val = b->data[i], offset = 1;

        a->data[i] = (a_val + b_val) % 10;
        carry = (a_val + b_val) / 10;

        while(carry)
        {
            if(i + offset >= a->length)
            {
                a->length++;
                if(a->length > a->capacity) number_realloc(a);
            }

            a_val = a->data[i + offset];
            a->data[i + offset] = (a_val + carry) % 10;
            carry = (a_val + carry) / 10;
            offset++;
        }
    }
}

void number_copy(Number *a, Number *b)
{
    while(a->length > b->capacity) number_realloc(b);
    b->length = a->length;
    for(int i = 0; i < a->length; i++) b->data[i] = a->data[i];
}
 
void print_number(Number *n)
{
    for(int i = n->length - 1; i >= 0; i--)
    {
        putchar('0' + n->data[i]);
    }
    putchar('\n');
}

int main()
{
    Number a, b;
    a.length = 1;
    a.capacity = 256;
    a.data = calloc(256, 1);
    a.data[0] = 2;

    b.capacity = 256;
    b.data = calloc(256, 1);

    number_copy(&a, &b);

    for(int i = 0; i < 999; i++)
    {
        decimal_number_add(&a, &b);
        number_copy(&a, &b);
    }

    unsigned sum = 0;
    for(int i = 0; i < a.length; i++) sum += a.data[i];
    printf("%u\n", sum);

    return 0;
}
