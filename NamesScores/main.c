#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>

typedef struct
{
    char *data;
    size_t length;
} String;

typedef struct
{
    String *data;
    size_t length;
    size_t capacity;
} StringArray;

void push(StringArray *arr, String s)
{
    if(arr->length >= arr->capacity)
    {
        arr->capacity *= 2;
        arr->data = (String *) realloc(arr->data, arr->capacity * sizeof(String));
    }

    arr->data[arr->length++] = s;
}

// Check if a is 'greater than' b i.e. would a be sorted after b in alphabetical
// order.
int string_compare(String a, String b)
{
    size_t shortest_length = a.length < b.length ? a.length : b.length;
    
    for(int i = 0; i < shortest_length; i++)
    {
        if(a.data[i] < b.data[i]) return 0;
        else if(b.data[i] < a.data[i]) return 1;
    }
    return shortest_length == b.length;
}

void swap(StringArray *s, int a, int b)
{
    String temp = s->data[a];
    s->data[a] = s->data[b];
    s->data[b] = temp;
}

void quick_sort(StringArray *arr, int s, int e)
{
    if(e > s)
    {
        int pivot = (s + e) / 2;
        String pivot_value = arr->data[pivot];

        swap(arr, pivot, e);
        int part_end = s;
        for(int i = s; i < e; i++)
        {
            if(string_compare(pivot_value, arr->data[i]))
            {
                swap(arr, part_end++, i);
            }
        }
        swap(arr, e, part_end);
        quick_sort(arr, s, part_end - 1);
        quick_sort(arr, part_end + 1, e);
    }
}

void print_string(String s)
{
    for(int i = 0; i < s.length; i++)
    {
        putchar(s.data[i]);
    }
    putchar('\n');
}

int main()
{
    size_t length = 0;
    struct stat file_info;
    stat("names.txt", &file_info);
    char *file_str = malloc(file_info.st_size);
    FILE *f = fopen("names.txt", "r");
    length = fread(file_str, 1, file_info.st_size, f);
    fclose(f);

    String s;
    StringArray arr;
    arr.data = (String *) malloc(256 * sizeof(String));
    arr.length = 0;
    arr.capacity = 256;
    
    for(int i = 0; i < length; i++)
    {
        if(file_str[i] != ',' && file_str[i] != '"')
        {
            s.data = file_str + i;
            s.length = 0;
            while(file_str[i] != ',' && file_str[i] != '"')
            {
                i++;
                s.length++;   
            }
            push(&arr, s);
        }
    }

    quick_sort(&arr, 0, arr.length - 1);
    uint64_t sum = 0;

    for(uint64_t i = 0; i < arr.length; i++)
    {
        String s = arr.data[i];
        uint64_t name_value = 0;
        for(int j = 0; j < s.length; j++)
        {
            name_value += s.data[j] - 'A' + 1;
        }
        sum += name_value * (i + 1);
    }

    printf("%lu\n", sum);

    return 0;
}
