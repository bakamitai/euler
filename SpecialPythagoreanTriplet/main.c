#include <stdio.h>

int main()
{
    int a = 1, b = 0, c = 0;

    for(;;)
    {
        b = a + 1;
        while((c = 1000 - a - b) > b)
        {
            if(a * a + b * b == c * c) goto end;
            b++;
        }
        a++;
    }
end:
    printf("%d + %d + %d = %d\n"
           "%d + %d = %d\n"
           "%d * %d * %d = %d\n",
           a, b, c, a + b + c,
           a * a, b * b, c * c,
           a, b, c, a * b * c);
    
    return 0;
}
