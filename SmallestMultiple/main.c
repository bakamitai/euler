#include <stdio.h>
#include <stdlib.h>

long is_prime(long n)
{
    if(n < 2) return 0;
    long factor = 2;

    while(factor * factor <= n)
    {
        if(n % factor == 0) return 0;
        factor++;
    }
    return 1;
}

long next_prime(long n)
{
    n -= 1 - n & 1;
    for(;;)
    {
        n += 2;
        if(is_prime(n)) return n;
    }
}

int factor(long n, int *factors)
{
    int length = 0;
    int prime = 2;
    while(n > 1)
    {
        if(n % prime == 0){
            n /= prime;
            factors[length++] = prime;
        }
        else prime = next_prime(prime);
    }
    return length;
}

int merge(int *p1, int l1, int *p2, int l2, int *p3)
{
    int c1 = 0, c2 = 0, c3 = 0;

    while(c1 < l1 || c2 < l2)
    {
        if(c1 >= l1) p3[c3++] = p2[c2++];
        else if(c2 >= l2) p3[c3++] = p1[c1++];
        else if(p1[c1] == p2[c2])
        {
            p3[c3++] = p1[c1];
            c1++;
            c2++;
        }
        else
        {
            while(c1 < l1 && p1[c1] < p2[c2]) p3[c3++] = p1[c1++];
            while(c2 < l2 && p2[c2] < p1[c1]) p3[c3++] = p2[c2++];
        }
    }
    return c3;
}

int main(int argc, char **argv)
{
    int a[20] = {0};
    int b[20] = {0};
    int c[20] = {0};

    int *part1 = a;
    int *part2 = b;
    int *part3 = c;
    int len1 = 0, len2 = 0, len3 = 0;
    
    *part1 = 2;
    *part2 = 3;
    len1 = 1;
    len2 = 1;
    len3 = merge(part1, len1, part2, len2, part3);

    for(int i = 4; i < 21; i++)
    {
        len1 = factor(i, part1);
        len2 = merge(part1, len1, part3, len3, part2);
        int *temp = part3;
        int temp_len = len3;
        part3 = part2;
        len3 = len2;
        part2 = temp;
        len2 = temp_len;
    }

    long product = 1;
    printf("Prime factorization.\n");
    for(int i = 0; i < len3; i++)
    {
        product *= part3[i];
        printf("%d ", part3[i]);
    }
    putchar('\n');
    printf("LCM = %ld\n", product);
    
    return 0;
}
