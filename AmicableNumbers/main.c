#include <stdio.h>

unsigned sum_of_divisors(unsigned n)
{
    unsigned sum = 1; // All numbers are divisible by 1
    unsigned div = 2;

    while(div * div < n)
    {
        if(n % div == 0)
        {
            sum += div + n / div;
        }
        div++;
    }
    if(div * div == n) sum += div;
    return sum;
}

int main()
{
    unsigned sum = 0;
    for(unsigned i = 1; i < 10000; i++)
    {
        unsigned s = sum_of_divisors(i);

        // Check that s != i to avoid including numbers that are equal to the sum
        // of their proper divisors e.g. sum_of_divisors(1) = 1
        if(s != i && sum_of_divisors(s) == i) sum += i;
    }

    printf("%u\n", sum);
    return 0;
}
