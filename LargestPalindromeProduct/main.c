#include <stdio.h>

int palindrome(int n)
{
    int result = 1;
    result &= (n / 100000 == n % 10);
    result &= (((n / 10000) % 10) == ((n / 10) % 10));
    result &= (((n / 1000) % 10) == ((n / 100) % 10));

    return result;
}

int main()
{
    int largest = 0;
    for(int i = 100; i < 1000; i++)
    {
        for(int j = 100; j < 1000; j++)
        {
            int candidate = i * j;
            if(palindrome(candidate) && candidate > largest) largest = candidate;
        }
    }
    printf("%d\n", largest);

    return 0;
}
