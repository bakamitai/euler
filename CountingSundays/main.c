#include <stdio.h>

int main()
{
    int months[12] = {
        31,  // January
        28,  // February
        31,  // March
        30,  // April
        31,  // May
        30,  // June
        31,  // July
        31,  // August
        30,  // September
        31,  // October
        30,  // November
        31   // December
    };
    int year = 1901;
    int days = 365;
    int sundays = days % 7 == 6;
    int month = 0;
    int leapyear = 0;

    while(year < 2001)
    {
        days += months[month++];
        if(month == 2 && leapyear) days++;
        sundays += days % 7 == 6;
        if(month > 11)
        {
            month = 0;
            year++;
            leapyear = (year % 100 && year % 4 == 0) || (year % 400 == 0);
        }
    }

    printf("%d\n", sundays);
    
    return 0;
}
