#include <stdio.h>

unsigned long is_prime(unsigned long n)
{
    if(n < 2) return 0;
    unsigned long factor = 2;

    while(factor * factor <= n)
    {
        if(n % factor == 0) return 0;
        factor++;
    }
    return 1;
}

unsigned long next_prime(unsigned long n)
{
    if(!(n & 1)) n--;
    for(;;)
    {
        n += 2;
        if(is_prime(n)) return n;
    }
}

int main()
{
    unsigned long sum = 0, prime = 2;
    while(prime < 2000000)
    {
        sum += prime;
        prime = next_prime(prime);
    }

    printf("%lu\n", sum);
    return 0;
}
