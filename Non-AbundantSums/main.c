#include <stdio.h>
#include <stdint.h>

uint64_t divisor_sum(uint64_t n)
{
    uint64_t sum = 1;
    uint64_t div = 2;

    while(div * div < n)
    {
        if(n % div == 0)
        {
            sum += div + n / div;
        }
        div++;
    }
    
    if(div * div == n) sum += div;

    return sum;
}

uint64_t next_abundant(uint64_t n)
{
    while(n >= divisor_sum(n)) n++;
    return n;
}

int main()
{
    uint64_t sum = 276;

    for(int i = 25; i < 28124; i++)
    {
        int abundant = 12;
        int abundant_sum = 0;
        while(2 * abundant <= i)
        {
            int remainder = i - abundant;
            if(remainder < divisor_sum(remainder)) 
            {
                abundant_sum = 1;
                break;
            }
            abundant = next_abundant(abundant + 1);
        }
        if(!abundant_sum) sum += i;
    }
    printf("%lu\n", sum);
    return 0;
}
