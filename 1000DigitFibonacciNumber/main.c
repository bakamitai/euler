#include <stdio.h>
#include <stdlib.h>

typedef struct
{
    char *data;
    size_t length;
    size_t capacity;
} Number;

void number_realloc(Number *n)
{
    n->capacity *= 2;
    n->data = realloc(n->data, n->capacity);
    for(int i = n->length; i < n->capacity; i++) n->data[i] = 0;
}

void decimal_number_add(Number *a, Number *b)
{
    for(int i = 0; i < b->length; i++)
    {
        int a_val = a->data[i], carry = 0, b_val = b->data[i], offset = 1;
        if(i >= a->length)
        {
            a->length++;
            if(a->length > a->capacity) number_realloc(a);
        }

        a->data[i] = (a_val + b_val) % 10;
        carry = (a_val + b_val) / 10;

        while(carry)
        {
            if(i + offset >= a->length)
            {
                a->length++;
                if(a->length > a->capacity) number_realloc(a);
            }

            a_val = a->data[i + offset];
            a->data[i + offset] = (a_val + carry) % 10;
            carry = (a_val + carry) / 10;
            offset++;
        }
    }
}

void number_copy(Number *a, Number *b)
{
    while(a->length > b->capacity) number_realloc(b);
    b->length = a->length;
    for(int i = 0; i < a->length; i++) b->data[i] = a->data[i];
}
 
void print_number(Number *n)
{
    for(int i = n->length - 1; i >= 0; i--)
    {
        putchar('0' + n->data[i]);
    }
    putchar('\n');
}

#define DIGITS 1000
int main()
{
    Number f1, f2;
    unsigned long index = 2;

    f1.data = calloc(1000, 1);
    f2.data = calloc(1000, 1);

    f1.length = 0;
    f2.length = 0;

    f1.capacity = 1000;
    f2.capacity = 1000;

    f1.data[0] = 1;
    f1.length = 1;

    f2.data[0] = 1;
    f2.length = 1;

    while(f1.length < DIGITS && f2.length < DIGITS)
    {
        index++;
        decimal_number_add(&f1, &f2);
        Number temp = f1;
        f1 = f2;
        f2 = temp;
    }
    printf("%lu\n", index);
    if(f1.length == DIGITS) print_number(&f1);
    else if(f2.length == DIGITS) print_number(&f2);
    return 0;
}
