#include <stdio.h>

int main()
{
    int term1 = 1, term2 = 2, term3 = 3, sum = 2;

    while(term3 < 4000000)
    {
        if(!(term3 & 1)) sum += term3;
        term1 = term2;
        term2 = term3;
        term3 = term1 + term2;
    }

    printf("%d\n", sum);
    return 0;
}
